# CBA goes to Pilchuck
 <img src="img/img5.png" width=700px >

Pilchuck will welcome this 2024 season the CBA as Scientist in Residence. We aim to bring machine-related processes to such a manual and (since the romans) unchanged craft as is glassblowing. 

There are several project we will aim to successfully fail and we will be reporting progress here. Before that, Quentin and I are exploring some glass related processes at at home, in the CBA.


# Friday May 3rd

We milled more glass, now 3D:
 <img src="img/img7.jpg" width=700px >
 <img src="img/img8.jpg" width=700px >
  <img src="img/img9.jpeg" width=700px >

We tried bubblegrams with our blue 40W laser but failed drastically: 

 <img src="img/img10.jpeg" width=700px >

 BUT we think we can still do a new approach to bubblegram but more like bubbletrapgram (BBTG)
 <img src="img/img11.jpeg" width=700px >
 <img src="img/img12.jpeg" width=700px >




# Friday April 19th
## Glass meet XTool 40W Laser Cutter

We were able to score and crack some shapes. This could be very interesting for murrine pickups with irregular tyling from sheet pannel. 

We are exploring the correct parameters in which we can little by little engrave a line, and then propagate a thermal crack that will follow the path of least resistance, hopefully, the damaged zone we previously scored. 

 <img src="img/img1.jpeg" width=700px >
  <img src="img/img2.jpeg" width=700px >
  <img src="img/img3.jpeg" width=700px >

## Glass meet CNC

The cheapes bit and a nice water pool didnt splash a single drop, all dust was enclosured as we were milling underwater and we successfully engraved a 2mm deep line . 

<img src="img/img4.jpeg" width=700px >
<img src="img/img6.jpeg" width=700px >